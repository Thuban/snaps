# snaps - play with OpenBSD snapshots
# See LICENSE for copyright

VERSION=1.0.1

PREFIX=/usr/local

changelog:
	@echo writing ChangeLog
	@git log --oneline --decorate > ChangeLog

man_clean:
	@echo cleaning snaps.1
	@rm -f snaps.1

man: man_clean
	@echo creating man page
	@txt2tags snaps.t2t

tarball: man
	@echo creating tarball
	@mkdir -p snaps-${VERSION}
	@cp -R AUTHORS LICENSE ChangeLog Makefile README.md snaps.t2t snaps.1 snaps snaps-${VERSION}
	@tar czvf snaps-${VERSION}.tar.gz snaps-${VERSION}
	@rm -rf snaps-${VERSION}

install:
	@echo installing executable file to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f snaps ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/snaps
	@echo installing man page to ${DESTDIR}${PREFIX}/usr/local/man/man1
	@mkdir -p ${DESTDIR}${PREFIX}/usr/local/man/man1
	@cp -f snaps.1 ${DESTDIR}${PREFIX}/usr/local/man/man1
	@chmod 444 ${DESTDIR}${PREFIX}/usr/local/man/man1/snaps.1

uninstall:
	@echo removing executable file from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/snaps
	@echo removing man page from ${DESTDIR}${PREFIX}/usr/local/man/man1
	@rm -f ${DESTDIR}${PREFIX}/usr/local/man/man1/snaps.1

.PHONY: man_clean man tarball install uninstall

