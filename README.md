# Snaps

An admin utility designed for the [OpenBSD operating system](http://www.openbsd.org) and published under the BSD licence.

## Purpose

It is made for users who use [-current](https://www.openbsd.org/faq/current.html) and jump from snapshot to snapshot.

It will help you to download the needed bsd.rd, check it and move it to the right place before rebooting.
A backup of your last bsd.rd is made. All steps are verbose.

It can be run with only the base system. Nevertheless if you install *curl*
from packages, it will check your $MIRROR build date of base and packages. You
will be able to decide if you want to do the update now or wait for the
synchronicity of the two directories.

## Usage

Snaps will use your `/etc/pkg.conf` file to setup automatically the `$MIRROR` variable.

You can also want to setup this variable manually:

```
$ MIRROR=http://another/openbsd/mirror doas snaps -G
```

Note that you just have to indicate the root of the MIRROR. This one
have to follow the OpenBSD [directory layout](http://www.openbsd.org/ftp.html#layout).

### -g

Fetch and check (SHA256 and signify) bsd.rd from $MIRROR (no need to be root for that one)

```
$ snaps -g
```

### -G

Fetch, check bsd.rd from $MIRROR then, move it to the right place, backup your last bsd.rd, print some reminders and reboot.

```
$ doas snaps -G
```

### -p

Update your -current port tree

```
$ doas snaps -p
```

### -l

List date and download places of the 3 last -G from /var/log/snaps.log

```
$ snaps -l
2016-04-30 22:16 FROM http://ftp.fr.openbsd.org/pub/OpenBSD/snapshots/amd64/
2016-05-10 11:03 FROM http://mirrors.ircam.fr/pub/OpenBSD/snapshots/amd64/
2016-05-14 11:11 FROM http://ftp.fr.openbsd.org/pub/OpenBSD/snapshots/amd64/
```

### -u

Upgrade snaps to the last release.

```
$ doas snaps -u
```

### -h

Print help and exit. It also print the content of the MIRROR variable based upon /etc/pkg.conf for a quick look.

```
$ snaps -h
```
