#!/bin/ksh
#
########################################################################
#
# snaps.sh - Upgrade your OpenBSD OS base to last -current snapshot
#
# Copyright (c) 2017, Fred Galusik <f AT galusik.xyz>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
########################################################################

###
### VARS
###

VERSION=1.0.3

if [ ! $MIRROR ]; then
    MIRROR=$(grep -v "^#.*" /etc/installurl)
fi
ARCH=$(uname -m)

MIRRORB=${MIRROR}/snapshots/${ARCH}/
MIRRORPKG=${MIRROR}/snapshots/packages/${ARCH}/

BSDRD=bsd.rd
SHAF=SHA256
SIGF=SHA256.sig

SIGNPUB=$(ls /etc/signify/openbsd-*-base.pub |tail -n1)
GET='ftp -n -m -C'
REBOOT='/sbin/reboot'

LOGF=/var/log/snaps.log
NOW=$(date "+ %Y-%m-%d %R")

REMOTESNAPS='https://framagit.org/fredg/snaps/raw/master/snaps'
LASTSNAPS=/tmp/snaps

###
###
###

toupgrade() {
    echo "\n=== NEW VERSION CHECKING ==="
    # some cleanup to not let ftp warn about not supporting resume
    if [[ -f $LASTSNAPS ]]; then
      rm -f $LASTSNAPS
    fi

    $GET -V -o "$LASTSNAPS" "$REMOTESNAPS"
    REMOTEVERSION=$(awk -F '=' '/^VERSION=/ { print $2 }' "$LASTSNAPS")

    if [ $? -eq 0 ]; then

        if [ "$VERSION" != "$REMOTEVERSION" ]; then
            echo "==> WARNING: snaps is not up-to-date !"
            echo "Last version: $REMOTEVERSION"
            echo "To upgrade, run 'snaps -u'"
        else
            echo "==> GOOD: snaps is up-to-date"
        fi

    else
          echo "==> WARNING: Remote snaps not reachable ! Check the connection."
    fi
}

upgrade() {
    SELFPATH=$(dirname $(readlink -f -- "$0"))
    echo "Downloading last snaps version"
    $GET -o "$SELFPATH" "$REMOTESNAPS"
}


usage() {
    echo ""
    echo "${0##*/} $VERSION"
    echo "Usage: ${0##*/} [-gGlh]"
    echo "  -g  fetch and check bsd.rd from \$MIRROR"
    echo "  -G  fetch, check and install bsd.rd from \$MIRROR"
    echo "  -p  update port tree to current"
    echo "  -l  list date and \$MIRROR of 3 last bsd.rd"
    echo "  -u  upgrade snaps to last version"
    echo "  -h  print this help and exit"
    echo ""
    echo "MIRROR is based upon /etc/installurl and set to: $MIRROR"
    echo ""
    echo "You can set it manually:"
    echo "\$ MIRROR=http://mirrors.ircam.fr/pub/OpenBSD doas snaps -G"
    echo ""
}

s_root() {
    if [[ $(id -u) != "0" ]]; then
        echo "==> WARNING: You need to be root to do this" 1>&2
        exit 1
    fi
}


s_log() {
    echo "$NOW FROM $MIRRORB" >> $LOGF
}


get_log() {
    if [[ -f $LOGF ]]; then
        tail -n 3 $LOGF
    else
        echo "No $LOGF file. Seems you have not yet played with snaps"
        exit 1
    fi
}


get_rd() {
    echo "\n=== FETCHING FILES ==="
    for _i in $BSDRD $SHAF $SIGF; do
        if [[ -f ${_i} ]]; then
            echo "\nDeleted old ${_i}"
            rm ${_i}
        fi
        echo "==> Fetching new ${_i}"
        $GET $MIRRORB${_i} || exit 1
    done
}


s_check() {
    echo "\n=== FILE INTEGRITY CHECKING ==="
    echo "Sign key: $SIGNPUB"
    OK=OK
    # signify
    if [[ $(signify -C -p $SIGNPUB -x $SIGF $BSDRD | awk '/'$BSDRD'/ {print $2}') = $OK ]]; then
        echo "==> GOOD: $SHAF file is coming from OpenBSD"
    else
        echo "==> WARNING: $SHAF is not coming from OpenBSD, aborting!!"
        exit 1
    fi
    # sha256
    if [[ $(sha256 -c $SHAF 2>&1 | awk '/'$BSDRD'/ {print $3}') = $OK ]]; then
        echo "==> GOOD: bsd.rd integrity checked"
    else
        echo "==> WARNING: bsd.rd seems corrupted, aborting!!"
        exit 1
    fi
}


s_move() {
    echo "\n=== MOVING FILE ==="
    cp -p /bsd /bsd.o || exit 1
    mv /bsd.rd /bsd.o.rd || exit 1
    mv $BSDRD / || exit 1
    rm $SHAF
    rm $SIGF
    echo "==> New bsd.rd installed and old files saved"
    echo "You can reboot your OS"
    echo "Type bsd.rd at the boot prompt, [enter] and choose (U)pgrade"
    echo ""
    echo "If something goes wrong, you can still boot your old kernel"
    echo "Just type bsd.o at the boot prompt"
    echo ""
    echo -n "==> Do you want to do it now ? (y/n) "
    read _a
    case ${_a} in
        y|Y)
            echo "After the upgrade, run sysmerge(8)"
            echo "Then, update packages or ports"
            echo "==> Rebooting..."
            $REBOOT
            ;;
        *)
            echo -n "Do you want to put the old bsd.rd back ? (y/n) "
            read _b
            case ${_b} in
                y|Y)
                    rm /bsd.rd || exit 1
                    rm /bsd.o || exit 1
                    mv /bsd.o.rd /bsd.rd || exit 1
                    echo "Done!"
                    exit 1
                    ;;
                *)
                    echo "Aborting..."
                    exit 1
                    ;;
            esac
            ;;
    esac
}

s_upports() {
    echo "\n=== UPDATE PORTS TREE ==="
    cd /usr/ports || exit 1
    cvs up -Pd || exit 1
    echo "==> New port tree fetched"
    echo ""
}

s_when() {
    # will only be done if curl from port/package is installed
    CURL=/usr/local/bin/curl
    if [[ -e $CURL ]]; then
        CURLB="$($CURL -sI ${MIRRORB}${SHAF} | grep Last-Modified)"
        CURLP="$($CURL -sI ${MIRRORPKG}${SHAF} | grep Last-Modified)"
        echo "\n=== BASE AND PACKAGES BUILD DATE ==="
        echo "From ${MIRROR}"
        echo "Base ($ARCH)     => $CURLB"
        echo "Packages ($ARCH) => $CURLP"
        echo ""
        echo -n "Do you want to continue ? (y/n): "
        read _c
        case ${_c} in
            y|Y)
                ;;
            *)
                echo "Aborting..."
                exit 1
                ;;
        esac
    fi
}

##
## Run
##

case $1 in
    -g)
        toupgrade
        get_rd
        s_check
        ;;
    -G)
        s_root
        toupgrade
        s_when
        s_log
        get_rd
        s_check
        s_move
        ;;
    -p)
        s_upports
        ;;
    -l)
        get_log
        ;;
    -u)
        upgrade
        ;;
    -h|*)
        usage
        exit 1
        ;;
esac
